import 'package:flutter/material.dart';
import 'package:simpleapp/homescreen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: Column(children: [
                Image.network(
                  'https://1.bp.blogspot.com/-hCOxpmzCFSA/XTeyJFHHjVI/AAAAAAAASUM/LgVfwPTc_xMo1NyH3i6yObsDJzxVwsC9gCLcBGAs/s1600/Gojek%2BTerbaru.png',
                  fit: BoxFit.cover,
                  height: 90,
                )
              ]),
              ),
              const Text(
                'Login',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                ),
              ),
              Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                obscureText: true,
                controller: emailController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(), labelText: 'Username or Email'
                ),
                ),
              ),
              Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                obscureText: true,
                controller: passwordController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(), labelText: 'Password'
                ),
                ),
              ),
              TextButton(
                style: TextButton.styleFrom(
                primary: Colors.green.shade600, // foreground
                ),
              onPressed: () {
                Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const HomeScreen())
                    );
              },
              child: Text('Login',
              style: TextStyle(
                fontSize: 16,
              ),), 
              )
            ],
          ),
        ),
      ),
    );
  }
}